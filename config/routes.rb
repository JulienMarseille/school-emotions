# frozen_string_literal: true

Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  devise_for :admin_users, ActiveAdmin::Devise.config
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "index#index"
  get "/map", to: "map#index"
  get "/tuto", to: "tuto#index"

  resources :planet
  get "/planet/end/:id", to: "planet#end"
end
