# frozen_string_literal: true

class Free < ApplicationRecord
  has_many :planet_frees
  has_many :planets, through: :planet_frees

  has_many :associations, dependent: :destroy
  accepts_nested_attributes_for :associations, allow_destroy: true
end
