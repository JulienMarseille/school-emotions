# frozen_string_literal: true

class Planet < ApplicationRecord
  has_one_attached :profil_picture
  has_one_attached :background_image

  has_many :planet_questions
  has_many :questions, through: :planet_questions
  accepts_nested_attributes_for :planet_questions, allow_destroy: true

  has_many :planet_memory_beginners
  has_many :memory_beginners, through: :planet_memory_beginners
  accepts_nested_attributes_for :planet_memory_beginners, allow_destroy: true

  has_many :planet_memory_hards
  has_many :memory_hards, through: :planet_memory_hards
  accepts_nested_attributes_for :planet_memory_hards, allow_destroy: true

  has_many :planet_body_reactions
  has_many :body_reactions, through: :planet_body_reactions
  accepts_nested_attributes_for :planet_body_reactions, allow_destroy: true

  has_many :planet_links
  has_many :links, through: :planet_links
  accepts_nested_attributes_for :planet_links, allow_destroy: true

  has_many :planet_connects
  has_many :connects, through: :planet_connects
  accepts_nested_attributes_for :planet_connects, allow_destroy: true

  has_many :planet_scratches
  has_many :scratches, through: :planet_scratches
  accepts_nested_attributes_for :planet_scratches, allow_destroy: true

  has_many :planet_frees
  has_many :frees, through: :planet_frees
  accepts_nested_attributes_for :planet_frees, allow_destroy: true
end
