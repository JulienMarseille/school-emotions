# frozen_string_literal: true

class Connect < ApplicationRecord
  has_many :planet_connects
  has_many :planets, through: :planet_connects

  has_one :definition, dependent: :destroy
  accepts_nested_attributes_for :definition, allow_destroy: true
end
