# frozen_string_literal: true

class MemoryHardChoice < ApplicationRecord
  has_one_attached :card_one
  has_one_attached :card_two

  belongs_to :memory_hard
end
