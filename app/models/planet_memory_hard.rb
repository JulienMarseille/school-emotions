# frozen_string_literal: true

class PlanetMemoryHard < ApplicationRecord
  belongs_to :planet, optional: true
  belongs_to :memory_hard, optional: true
end
