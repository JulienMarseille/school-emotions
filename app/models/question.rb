# frozen_string_literal: true

class Question < ApplicationRecord
  has_many :planet_questions
  has_many :planets, through: :planet_questions

  has_many :choices, dependent: :destroy
  accepts_nested_attributes_for :choices, allow_destroy: true
end
