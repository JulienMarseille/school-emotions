# frozen_string_literal: true

class Reaction < ApplicationRecord
  has_one_attached :physic_reaction

  belongs_to :emotion
end
