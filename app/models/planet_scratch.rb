# frozen_string_literal: true

class PlanetScratch < ApplicationRecord
  belongs_to :planet, optional: true
  belongs_to :scratch, optional: true
end
