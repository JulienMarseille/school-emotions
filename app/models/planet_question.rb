# frozen_string_literal: true

class PlanetQuestion < ApplicationRecord
  belongs_to :planet, optional: true
  belongs_to :question, optional: true
end
