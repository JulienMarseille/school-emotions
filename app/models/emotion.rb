# frozen_string_literal: true

class Emotion < ApplicationRecord
  has_one_attached :body

  belongs_to :body_reaction

  has_many :reactions, dependent: :destroy
  accepts_nested_attributes_for :reactions, allow_destroy: true
end
