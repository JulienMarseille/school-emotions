# frozen_string_literal: true

class GroupEmotion < ApplicationRecord
  has_one_attached :body
  has_one_attached :emotion_file_one
  has_one_attached :emotion_file_two

  belongs_to :scratch
end
