# frozen_string_literal: true

class MemoryHard < ApplicationRecord
  has_many :planet_memory_hards
  has_many :planets, through: :planet_memory_hards

  has_many :memory_hard_choices, dependent: :destroy
  accepts_nested_attributes_for :memory_hard_choices, allow_destroy: true
end
