# frozen_string_literal: true

class PlanetMemoryBeginner < ApplicationRecord
  belongs_to :planet, optional: true
  belongs_to :memory_beginner, optional: true
end
