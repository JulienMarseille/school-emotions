# frozen_string_literal: true

class PlanetConnect < ApplicationRecord
  belongs_to :planet, optional: true
  belongs_to :connect, optional: true
end
