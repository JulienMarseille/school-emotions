# frozen_string_literal: true

class PlanetLink < ApplicationRecord
  belongs_to :planet, optional: true
  belongs_to :link, optional: true
end
