# frozen_string_literal: true

class PlanetBodyReaction < ApplicationRecord
  belongs_to :planet, optional: true
  belongs_to :body_reaction, optional: true
end
