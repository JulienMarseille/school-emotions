# frozen_string_literal: true

class MemoryBeginner < ApplicationRecord
  has_many :planet_memory_beginners
  has_many :planets, through: :planet_memory_beginners

  has_many :memory_beginner_choices, dependent: :destroy
  accepts_nested_attributes_for :memory_beginner_choices, allow_destroy: true
end
