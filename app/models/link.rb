# frozen_string_literal: true

class Link < ApplicationRecord
  has_many :planet_links
  has_many :planets, through: :planet_links

  has_many :groups, dependent: :destroy
  accepts_nested_attributes_for :groups, allow_destroy: true
end
