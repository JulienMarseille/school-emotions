# frozen_string_literal: true

class BodyReaction < ApplicationRecord
  has_many :planet_body_reactions
  has_many :planets, through: :planet_body_reactions

  has_one :emotion, dependent: :destroy
  accepts_nested_attributes_for :emotion, allow_destroy: true
end
