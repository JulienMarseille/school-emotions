# frozen_string_literal: true

class MemoryBeginnerChoice < ApplicationRecord
  has_one_attached :card

  belongs_to :memory_beginner
end
