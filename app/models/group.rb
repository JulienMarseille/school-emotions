# frozen_string_literal: true

class Group < ApplicationRecord
  has_one_attached :child
  has_one_attached :child_mad
  has_one_attached :object

  belongs_to :link
end
