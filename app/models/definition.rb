# frozen_string_literal: true

class Definition < ApplicationRecord
  has_one_attached :emotion_image

  belongs_to :connect
end
