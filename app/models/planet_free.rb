# frozen_string_literal: true

class PlanetFree < ApplicationRecord
  belongs_to :planet, optional: true
  belongs_to :free, optional: true
end
