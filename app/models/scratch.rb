# frozen_string_literal: true

class Scratch < ApplicationRecord
  has_many :planet_scratches
  has_many :planets, through: :planet_scratches

  has_one :group_emotion, dependent: :destroy
  accepts_nested_attributes_for :group_emotion, allow_destroy: true
end
