# frozen_string_literal: true

class MapController < ApplicationController
  def index
    @planets = Planet.all.order("id")
  end
end
