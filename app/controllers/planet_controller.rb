# frozen_string_literal: true

class PlanetController < ApplicationController
  def show
    @planet = Planet.find(params[:id])
    @questions = @planet.questions.all
    @memory_beginners = @planet.memory_beginners.all
    @memory_hards = @planet.memory_hards.all
    @body_reactions = @planet.body_reactions.all
    @links = @planet.links.all
    @connects = @planet.connects.all
    @scratches = @planet.scratches.all
    @frees = @planet.frees.all
  end

  def end
    @planet = Planet.find(params[:id])
  end
end
