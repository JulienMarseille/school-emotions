# frozen_string_literal: true

module IndexHelper
  def show_svg(path)
    File.open(path, "rb") do |file|
      raw file.read
    end
    end
end
