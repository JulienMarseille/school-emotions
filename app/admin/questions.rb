# frozen_string_literal: true

ActiveAdmin.register Question do
  menu label: "Imagine tes émotions"

  permit_params :name, :entitled, :situation, :explanation_title, :explanation,
  choices_attributes:
  [
    :id,
    :answer,
    :true,
    :_destroy
  ]

  form do |f|
    f.semantic_errors
    f.object.errors.keys
    f.input :name
    f.input :entitled
    f.input :situation
    f.input :explanation_title
    f.input :explanation
    f.inputs do
      f.has_many :choices do |t|
        t.input :answer, collection: ListEmotion.all.map { |v| [v.name, v.name] }
        t.input :true, as: :boolean
      end
    end
    f.actions
  end
end
