# frozen_string_literal: true

ActiveAdmin.register BodyReaction do
  menu label: "Le corps en action"

  permit_params :name, :entitled,
  emotion_attributes:
  [
    :id,
    :name,
    :body,
    :_destroy,
    reactions_attributes:
    [
      :id,
      :name,
      :physic_reaction,
      :description,
      :_destroy
    ]
  ]

  form do |f|
    f.semantic_errors
    f.object.errors.keys
    f.inputs :name
    f.inputs :entitled
    f.inputs do
      f.has_many :emotion, class: "has_one" do |t|
        t.input :name
        t.input :body, as: :file
        t.has_many :reactions do |i|
          i.input :name, collection: MembreCorp.all.map { |v| [v.name, v.name] }
          i.input :physic_reaction, as: :file
          i.input :description
        end
      end
    end

    f.actions
  end
end
