# frozen_string_literal: true

ActiveAdmin.register Connect do
  menu label: "Je me sens..."

  permit_params :name, :entitled,
  definition_attributes:
  [
    :id,
    :entitled,
    :text,
    :emotion_image,
    :_destroy
  ]

  form do |f|
    f.semantic_errors
    f.object.errors.keys
    f.input :name
    f.input :entitled
    f.inputs do
    f.has_many :definition, class: "has_one" do |t|
      t.input :entitled
      t.input :text, collection: ListEmotion.all.map { |v| [v.name, v.name] }
      t.input :emotion_image, as: :file
    end
  end
    f.actions
  end
end
