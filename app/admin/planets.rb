# frozen_string_literal: true

ActiveAdmin.register Planet do
  menu priority: 1, label: "Les planètes"

  permit_params :name, :profil_picture, :background_image, :end_text,
  planet_questions_attributes:
  [
    :id,
    :question_id,
    :_destroy
  ],
  planet_memory_beginners_attributes:
  [
    :id,
    :memory_beginner_id,
    :_destroy
  ],
  planet_memory_hards_attributes:
  [
    :id,
    :memory_hard_id,
    :_destroy
  ],
  planet_body_reactions_attributes:
  [
    :id,
    :body_reaction_id,
    :_destroy
  ],
  planet_links_attributes:
  [
    :id,
    :link_id,
    :_destroy
  ],
  planet_connects_attributes:
  [
    :id,
    :connect_id,
    :_destroy
  ],
  planet_scratches_attributes:
  [
    :id,
    :scratch_id,
    :_destroy
  ],
  planet_frees_attributes:
  [
    :id,
    :free_id,
    :_destroy
  ]

  form do |f|
    f.semantic_errors
    f.object.errors.keys
    f.input :name
    f.input :profil_picture, as: :file
    f.input :background_image, as: :file
    f.input :end_text

    f.has_many :planet_questions, heading: "Imagines tes émotions" do |t|
      t.input :question, collection: Question.all
    end

    f.has_many :planet_memory_beginners, heading: "Le mémory des émotions 1" do |t|
      t.input :memory_beginner, collection: MemoryBeginner.all
    end

    f.has_many :planet_memory_hards, heading: "Le mémory des émotions 2" do |t|
      t.input :memory_hard, collection: MemoryHard.all
    end

    f.has_many :planet_body_reactions, heading: "Le corps en action" do |t|
      t.input :body_reaction, collection: BodyReaction.all
    end

    f.has_many :planet_links, heading: "J'aime ou j'aime pas" do |t|
      t.input :link, collection: Link.all
    end

    f.has_many :planet_connects, heading: "Je me sens..." do |t|
      t.input :connect, collection: Connect.all
    end

    f.has_many :planet_scratches, heading: "Emotions masquées" do |t|
      t.input :scratch, collection: Scratch.all
    end

    f.has_many :planet_frees, heading: "Libérons tes émotions" do |t|
      t.input :free, collection: Free.all
    end

    f.actions
  end
end
