# frozen_string_literal: true

ActiveAdmin.register Link do
  menu label: "J’aime ou j’aime pas"

  permit_params :name, :entitled,
  groups_attributes:
  [
    :id,
    :child,
    :child_mad,
    :object,
    :solution,
    :error,
    :_destroy
  ]

  form do |f|
    f.semantic_errors
    f.object.errors.keys
    f.input :name
    f.input :entitled
    f.inputs do
      f.has_many :groups do |t|
        t.input :child, as: :file
        t.input :child_mad, as: :file
        t.input :object, as: :file
        t.input :solution
        t.input :error
      end
    end
    f.actions
  end
end
