# frozen_string_literal: true

ActiveAdmin.register Free, as: "All Free" do
  menu label: "Libérons tes émotions"

  permit_params :name, :entitled,
  associations_attributes:
  [
    :id,
    :emotion,
    :definition,
    :solution,
    :image,
    :_destroy
  ]

  form do |f|
    f.semantic_errors
    f.object.errors.keys
    f.input :name
    f.input :entitled
    f.inputs do
      f.has_many :associations do |t|
        t.input :emotion, collection: ListEmotion.all.map { |v| [v.name, v.name] }
        t.input :definition
        t.input :solution
        t.input :image, as: :file
      end
    end
    f.actions
  end
end
