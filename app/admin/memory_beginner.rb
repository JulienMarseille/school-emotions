# frozen_string_literal: true

ActiveAdmin.register MemoryBeginner do
  menu label: "Le mémory des émotions 1"

  permit_params :name, :entitled,
  memory_beginner_choices_attributes:
  [
    :id,
    :answer,
    :answer_sub,
    :card,
    :_destroy
  ]

  form do |f|
    f.semantic_errors
    f.object.errors.keys
    f.inputs :name
    f.inputs :entitled
    f.inputs do
      f.has_many :memory_beginner_choices do |t|
        t.input :card, as: :file
        t.input :answer
        t.input :answer_sub
      end
    end

    f.actions
  end
end
