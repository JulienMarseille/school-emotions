# frozen_string_literal: true

ActiveAdmin.register Scratch do
  menu label: "Emotions masquées"

  permit_params :name, :entitled,
  group_emotion_attributes:
  [
    :id,
    :body,
    :situation,
    :emotion_file_one,
    :emotion_file_two,
    :emotion,
    :solution_title,
    :solution_subtitle,
    :_destroy
  ]

  form do |f|
    f.semantic_errors
    f.object.errors.keys
    f.input :name
    f.input :entitled
    f.inputs do
      f.has_many :group_emotion, class: "has_one" do |t|
        t.input :body, as: :file
        t.input :situation
        t.input :emotion_file_one, as: :file
        t.input :emotion_file_two, as: :file
        t.input :emotion
        t.input :solution_title
        t.input :solution_subtitle
      end
    end
    f.actions
  end
end
