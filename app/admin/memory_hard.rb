# frozen_string_literal: true

ActiveAdmin.register MemoryHard do
  menu label: "Le mémory des émotions 2"

  permit_params :name, :entitled,
  memory_hard_choices_attributes:
  [
    :id,
    :card_one,
    :card_two,
    :emotion_name,
    :emotion_emotion,
    :_destroy
  ]

  form do |f|
    f.semantic_errors
    f.object.errors.keys
    f.inputs :name
    f.inputs :entitled
    f.inputs do
      f.has_many :memory_hard_choices do |t|
        t.input :card_one, as: :file
        t.input :card_two, as: :file
        t.input :emotion_name
        t.input :emotion_description
      end
    end

    f.actions
  end
end
