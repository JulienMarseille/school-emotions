window.addEventListener("turbolinks:load", function() {

    game_state = 0;
    cards = document.getElementsByClassName("gameContainer");
    next = document.getElementById("next_connect")
    nexts = document.getElementsByClassName("next");
        
    for (let next of nexts) {
        next.addEventListener("click", function(){
            next_game(cards)  
            clear(question)
        });
    }



    next_game(cards);



    //// MEMOMRY /////

    memory = Array.from(document.getElementsByClassName("memory"))

    if (memory != null && memory.length > 0){

        iterator = 2

        memoryCards = document.getElementsByClassName("memoryCard");

        for (var i = 0; i < memoryCards.length; i++) {
            memoryCards[i].addEventListener("click", function(){

                this.classList.add('active');   
                    

                if (document.getElementsByClassName("active")[0].id == document.getElementsByClassName("active")[1].id){
                    document.getElementsByClassName("active")[0].classList.add('done');
                    document.getElementsByClassName("active")[1].classList.add('done');

                    document.getElementsByClassName("active")[0].classList.remove('active');
                    document.getElementsByClassName("active")[0].classList.remove('active');

                    $("#popup-" + this.getAttribute('id') ).addClass("block")
                    $("#popup-" + this.getAttribute('id') ).removeClass("none")

                    if (document.getElementsByClassName("done").length == document.getElementsByClassName("memoryCard").length) {
                        $("#active").find("#next_connect").removeClass("none");
                    }

                } else {

                document.getElementsByClassName("active")[0].classList.add('animate');   
                document.getElementsByClassName("active")[1].classList.add('animate');   
                    setTimeout(function(){
                        document.getElementsByClassName("active")[0].classList.remove('animate');
                        document.getElementsByClassName("active")[1].classList.remove('animate');
                        document.getElementsByClassName("active")[0].classList.remove('active');
                        document.getElementsByClassName("active")[0].classList.remove('active');
                    }, 1500);
                }

                backs = Array.from(document.getElementsByClassName("back"))
                    
                backs.forEach(back =>{
                    back.addEventListener("click", function(){
                        $(this).closest(".popup").addClass("none")
                        $(this).closest(".popup").removeClass("block")
                    });
                });


                //document.getElementsByClassName("active")[0].classList.remove('active');


            });
        }
        
    }





    /////// Libérons tes emotions ///////  

    free = document.getElementsByClassName("free")

    if (free != null && free.length > 0){

        $(".solution").draggable()


        $( ".slot" ).droppable({
            accept: ".solution",
            activeClass: "ui-state-hover",
            hoverClass: "ui-state-active",
            drop: function( event, ui ) {

                if (this.getAttribute('id') == document.getElementsByClassName("ui-draggable-dragging")[0].getAttribute('id') ){

                    // Afficher l'écran bravo
                    $("#popup-" + this.getAttribute('id') ).addClass("block")
                    $("#popup-" + this.getAttribute('id') ).removeClass("none")


                    backs = Array.from(document.getElementsByClassName("back"))
                    
                    backs.forEach(back =>{
                        back.addEventListener("click", function(){
                            $(this).closest(".popup").addClass("none")
                            $(this).closest(".popup").removeClass("block")
                        });
                    });
                    

                    $(this).addClass( "done" )


                    if (document.getElementsByClassName("done").length == document.getElementsByClassName("solution").length) {
                        $("#active").find("#next_connect").removeClass("none");
                    }
                                    
                
                } else {

                    document.getElementsByClassName("ui-draggable-dragging")[0].classList.add('animate');   
                    setTimeout(function(){
                        document.getElementsByClassName("animate")[0].classList.remove('animate');
                    }, 1000);
            
                }
            }
        });
    }

    /////// Like unlike ///////  

    likeUnlike = document.getElementsByClassName("likeUnlike")

    if (likeUnlike != null && likeUnlike.length > 0) {

        $(".object").draggable()

        $( ".child" ).droppable({
            accept: ".object",
            hoverClass: "ui-hover-like",
            drop: function( event, ui ) {

                if (this.getAttribute('id') == document.getElementsByClassName("ui-draggable-dragging")[0].getAttribute('id') ){
                    

                    // Afficher l'écran bravo
                    $("#popup-" + this.getAttribute('id') ).addClass("block")
                    $("#popup-" + this.getAttribute('id') ).removeClass("none")


                    backs = Array.from(document.getElementsByClassName("back"))
                    
                    backs.forEach(back =>{
                        back.addEventListener("click", function(){
                            $(this).closest(".popup").addClass("none")
                            $(this).closest(".popup").removeClass("block")
                        });
                    });
                    

                    $(this).addClass( "done" )


                    if (document.getElementsByClassName("done").length == document.getElementsByClassName("object").length) {
                        $("#active").find("#next_connect").removeClass("none");
                    }
                
                } else {

                    $("#popup-wrong-" + this.getAttribute('id') ).addClass("block")
                    $("#popup-wrong-" + this.getAttribute('id') ).removeClass("none")


                    backs = Array.from(document.getElementsByClassName("back"))
                    
                    backs.forEach(back =>{
                        back.addEventListener("click", function(){
                            $(this).closest(".popup-wrong").addClass("none")
                            $(this).closest(".popup-wrong").removeClass("block")
                        });
                    });              
                }
            }
        });
    }

    /////// Scratch ///////  

    maskedEmotions= document.getElementsByClassName("maskedEmotions")

    if (maskedEmotions != null && maskedEmotions.length > 0){

        emotions = document.getElementsByClassName("emotion")
        clouds = Array.from(document.getElementsByClassName("cloudContainer"))       

        clouds.forEach(cloud =>{
            opacity = 0;
            cloud.addEventListener("click", function(){
                for (var i = 0; i < emotions.length; i++) {
                    
                    emotions[i].style.opacity = opacity ;
                }
                opacity = opacity + 0.05;

                if (opacity > 1.2){
                    console.log("ici")
                    $("#active .popup").removeClass("none")
                    $("#active").find("#next_connect").removeClass("none");     
                }
            })
        })
    }

    /////// Corps en action ///////  

    child = document.getElementById("active");
    bodyAction = document.getElementsByClassName("bodyAction")

    

    if (bodyAction != null && bodyAction.length > 0){
        displayLink()

        $(".reactionGroup").draggable()

        $( ".emotionLink" ).droppable({
            accept: ".reactionGroup",
            hoverClass: "ui-hover-like",
            drop: function( event, ui ) {
                
                if (this.getAttribute('id') == document.getElementsByClassName("ui-draggable-dragging")[0].getAttribute('id') ){             

                    $(this).addClass( "done" )

                    if (document.getElementsByClassName("done").length == $('#active').find('.reactionGroup').length) {

                        $('#active').find("#next_connect").removeClass("none")
                    }
                                    
                
                } else {

                    document.getElementsByClassName("ui-draggable-dragging")[0].classList.add('animate');   
                    setTimeout(function(){
                        document.getElementsByClassName("animate")[0].classList.remove('animate');
                    }, 1000);
            
                }

            }
        });
    }


    /////// CONNECT ///////  

     connect = document.getElementsByClassName("connect")

    if (connect != null && connect.length > 0){

        reponses = document.getElementsByClassName("emotionButton")

        
        for (let reponse of reponses) {
            reponse.addEventListener("click", function(){
                if ($("#active").find(".definition").attr("id").toUpperCase() == reponse.textContent.toUpperCase() ){
                    $("#active").find("#next_connect").removeClass("none");
                    reponse.style.backgroundColor = "rgb(212, 255, 195)";
                } else {
                    reponse.classList.add('shake-question');   
                    
                    setTimeout(function(){
                        reponse.classList.remove('shake-question');
                    }, 1000);
                }
            })
        }
    }


    /////// Mini-jeu quizz de situation ///////  

    question = document.getElementsByClassName("question")

    if (question != null && question.length > 0){

        solutions = document.getElementsByClassName("solution")
        answer_explication = document.getElementsByClassName("answer-explication")

        for (let solution of solutions) {
            solution.addEventListener("click", function(){

                // bonne rep
                if (solution.getAttribute('id') == "true"){
                    for (var i = 0; i < answer_explication.length; i++) {
                        answer_explication[i].classList.remove("none")
                    }
                    for (var i = 0; i < question.length; i++) {
                        question[i].classList.add("none")
                    }
                    question[0].classList.add('none'); 

                //mauvaise rep
                } else {
                    solution.classList.add('animate');   
                    setTimeout(function(){
                        solution.classList.remove('animate');
                    }, 1000);
                }
            });
        }
    }



    //// fin des mini-jeux




})

function displayLink() {
    $('#active').find('#reactions').find( "div" ).each(function( index ) {
            
        let info = this.id

        $('#active img').each(function( index ) {
            if (this.id == info){
                this.style.display = "block"
                return
            }
        })

    })
    
}


function next_game(cards) { 
    for (i = 0; i < cards.length; i++) {
        cards[i].style.display = 'block';   
        cards[i].setAttribute("id", "active");
        if (i === game_state) { 
            continue; 
        }
        cards[i].style.display = 'none';   
        cards[i].removeAttribute("id");
    }

    game_state = game_state + 1
    $(".state").text(game_state)
    $(".stateEnd").text(cards.length)
} 

function clear(question = null){

    if (game_state == cards.length + 1){
        $("#cross").addClass("none")
        $(".bravo").addClass("active")
        setTimeout(function(){
            $(".bravo").removeClass("active")
        }, 2700);
        setTimeout(function(){
            $(".bravo").removeClass("active")
        }, 3100);
        setTimeout(function(){
            url = window.location.href.split('/');
            lastSegment = url.pop() || url.pop()
            window.location.replace("/planet/end/" + lastSegment);
        }, 3300);

    }

    if (question != null && question.length > 0){
        for (var i = 0; i < answer_explication.length; i++) {
            answer_explication[i].classList.add("none")
        }
        for (var i = 0; i < question.length; i++) {
            question[i].classList.remove("none")
        }

    }

    if (maskedEmotions != null && maskedEmotions.length > 0 ){
        emotions = Array.from(document.getElementsByClassName("emotion"))

        for (var i = 0; i < emotions.length; i++) {
            emotions[i].style.opacity = 0;
        }
        opacity = 0;

    }

    if (bodyAction != null && bodyAction.length > 0){
        displayLink()

        dones = Array.from(document.getElementsByClassName("done"))
        for (var i = 0; i < dones.length; i++) {
            dones[i].classList.remove('done')
        }

    }   

}


