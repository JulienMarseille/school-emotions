// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require_tree .
//= require game
//= require jquery-ui



window.addEventListener("turbolinks:load", function() {

    const planets = document.querySelectorAll('.planet');
    planets.forEach((planet, i) => {
        if (i === 0)
            planet.classList.add('center');

        else if (i === 1)
            planet.classList.add('right');
        else
            planet.classList.add('rightOut');
    });

    window.requestAnimationFrame(() => {
        document.body.classList.remove('noAnimation');
    })

    const arrowRight = document.querySelector('.arrowRight');
    
    arrowRight.onclick = e => {
        const left = document.querySelector('.left');
        const center = document.querySelector('.center');
        const right = document.querySelector('.right');
        const rightOut = document.querySelector('.rightOut');

        console.log('itbuilds');

        if (!right)
            return;
        
        if (left) {
            left.classList.remove('left');
            left.classList.add('leftOut');
        }
    
        center.classList.remove('center');
        center.classList.add('left');
    
        right.classList.remove('right');
        right.classList.add('center');

        if (rightOut) {
            rightOut.classList.remove('rightOut');
            rightOut.classList.add('right');
        }
    };

    const arrowLeft = document.querySelector('.arrowLeft');

    arrowLeft.onclick = e => {
        const left = document.querySelector('.left');
        const right = document.querySelector('.right');
        const center = document.querySelector('.center');

        if (!left)
            return;

        left.classList.remove('left');
        left.classList.add('center');
        
        center.classList.remove('center');
        center.classList.add('right');

        if (right) {
            right.classList.remove('right');
            right.classList.add('rightOut');
        }

        const leftOuts = document.querySelectorAll('.leftOut');

        if (leftOuts.length > 0) {
            const leftOut = leftOuts[leftOuts.length - 1];
            leftOut.classList.remove('leftOut');
            leftOut.classList.add('left');
        }
    };
    
    const helpButton = document.querySelector('.helpButton');
    const closeButton = document.querySelector('.closeButton');
    const emotionsPopup = document.querySelector('.emotionsPopup');

    helpButton.onclick = e => {
        emotionsPopup.classList.add('active');
    }

    closeButton.onclick = e => {
        emotionsPopup.classList.remove('active');
    }
});