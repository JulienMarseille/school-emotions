# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_04_17_163945) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.bigint "resource_id"
    t.string "author_type"
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "associations", force: :cascade do |t|
    t.string "emotion"
    t.string "definition"
    t.bigint "free_id"
    t.string "solution"
    t.index ["free_id"], name: "index_associations_on_free_id"
  end

  create_table "body_reactions", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "entitled"
  end

  create_table "choices", force: :cascade do |t|
    t.string "answer"
    t.boolean "true"
    t.bigint "question_id"
    t.string "answer_sub"
    t.index ["question_id"], name: "index_choices_on_question_id"
  end

  create_table "connects", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "entitled"
  end

  create_table "definitions", force: :cascade do |t|
    t.string "entitled"
    t.string "text"
    t.bigint "connect_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["connect_id"], name: "index_definitions_on_connect_id"
  end

  create_table "emotions", force: :cascade do |t|
    t.string "name"
    t.bigint "body_reaction_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["body_reaction_id"], name: "index_emotions_on_body_reaction_id"
  end

  create_table "frees", force: :cascade do |t|
    t.string "name"
    t.string "entitled"
  end

  create_table "group_emotions", force: :cascade do |t|
    t.string "emotion"
    t.bigint "scratch_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "situation"
    t.string "solution_title"
    t.string "solution_subtitle"
    t.index ["scratch_id"], name: "index_group_emotions_on_scratch_id"
  end

  create_table "groups", force: :cascade do |t|
    t.string "solution"
    t.string "error"
    t.bigint "link_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["link_id"], name: "index_groups_on_link_id"
  end

  create_table "links", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "entitled"
  end

  create_table "list_emotions", force: :cascade do |t|
    t.string "name"
  end

  create_table "membre_corps", force: :cascade do |t|
    t.string "name"
  end

  create_table "memory_beginner_choices", force: :cascade do |t|
    t.string "answer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "memory_beginner_id"
    t.string "answer_sub"
    t.index ["memory_beginner_id"], name: "index_memory_beginner_choices_on_memory_beginner_id"
  end

  create_table "memory_beginners", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "entitled"
  end

  create_table "memory_hard_choices", force: :cascade do |t|
    t.bigint "memory_hard_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "emotion_name"
    t.string "emotion_description"
    t.index ["memory_hard_id"], name: "index_memory_hard_choices_on_memory_hard_id"
  end

  create_table "memory_hards", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "entitled"
  end

  create_table "planet_body_reactions", force: :cascade do |t|
    t.bigint "planet_id"
    t.bigint "body_reaction_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["body_reaction_id"], name: "index_planet_body_reactions_on_body_reaction_id"
    t.index ["planet_id"], name: "index_planet_body_reactions_on_planet_id"
  end

  create_table "planet_connects", force: :cascade do |t|
    t.bigint "planet_id"
    t.bigint "connect_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["connect_id"], name: "index_planet_connects_on_connect_id"
    t.index ["planet_id"], name: "index_planet_connects_on_planet_id"
  end

  create_table "planet_frees", force: :cascade do |t|
    t.bigint "planet_id"
    t.bigint "free_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["free_id"], name: "index_planet_frees_on_free_id"
    t.index ["planet_id"], name: "index_planet_frees_on_planet_id"
  end

  create_table "planet_links", force: :cascade do |t|
    t.bigint "planet_id"
    t.bigint "link_id"
    t.index ["link_id"], name: "index_planet_links_on_link_id"
    t.index ["planet_id"], name: "index_planet_links_on_planet_id"
  end

  create_table "planet_memory_beginners", force: :cascade do |t|
    t.bigint "planet_id"
    t.bigint "memory_beginner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["memory_beginner_id"], name: "index_planet_memory_beginners_on_memory_beginner_id"
    t.index ["planet_id"], name: "index_planet_memory_beginners_on_planet_id"
  end

  create_table "planet_memory_hards", force: :cascade do |t|
    t.bigint "planet_id"
    t.bigint "memory_hard_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["memory_hard_id"], name: "index_planet_memory_hards_on_memory_hard_id"
    t.index ["planet_id"], name: "index_planet_memory_hards_on_planet_id"
  end

  create_table "planet_questions", force: :cascade do |t|
    t.bigint "planet_id"
    t.bigint "question_id"
    t.index ["planet_id"], name: "index_planet_questions_on_planet_id"
    t.index ["question_id"], name: "index_planet_questions_on_question_id"
  end

  create_table "planet_scratches", force: :cascade do |t|
    t.bigint "planet_id"
    t.bigint "scratch_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["planet_id"], name: "index_planet_scratches_on_planet_id"
    t.index ["scratch_id"], name: "index_planet_scratches_on_scratch_id"
  end

  create_table "planets", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "end_text"
  end

  create_table "questions", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "entitled"
    t.string "situation"
    t.string "explanation"
    t.string "explanation_title"
  end

  create_table "reactions", force: :cascade do |t|
    t.string "name"
    t.bigint "emotion_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "description"
    t.index ["emotion_id"], name: "index_reactions_on_emotion_id"
  end

  create_table "scratches", force: :cascade do |t|
    t.string "name"
    t.string "entitled"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
end
