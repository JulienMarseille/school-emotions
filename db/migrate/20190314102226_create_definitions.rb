class CreateDefinitions < ActiveRecord::Migration[5.2]
  def change
    create_table :definitions do |t|
      t.string :entitled
      t.string :text

      t.references :connect
      t.timestamps
    end
  end
end
