class AddSolutionToAssociation < ActiveRecord::Migration[5.2]
  def change
    add_column :associations, :solution, :string
  end
end
