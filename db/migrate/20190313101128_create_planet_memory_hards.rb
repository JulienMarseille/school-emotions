class CreatePlanetMemoryHards < ActiveRecord::Migration[5.2]
  def change
    create_table :planet_memory_hards do |t|
      t.references :planet
      t.references :memory_hard

      t.timestamps
    end
  end
end
