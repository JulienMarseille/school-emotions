class CreateAssociation < ActiveRecord::Migration[5.2]
  def change
    create_table :associations do |t|
      t.string :emotion
      t.string :definition

      t.references :free
    end
  end
end
