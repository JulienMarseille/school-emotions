class AddSituationToQuestion < ActiveRecord::Migration[5.2]
  def change
    add_column :questions, :situation, :string
  end
end
