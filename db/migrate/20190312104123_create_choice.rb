class CreateChoice < ActiveRecord::Migration[5.2]
  def change
    create_table :choices do |t|
      t.string :answer
      t.boolean :true
    end
  end
end
