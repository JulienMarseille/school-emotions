class CreatePlanetBodyReactions < ActiveRecord::Migration[5.2]
  def change
    create_table :planet_body_reactions do |t|
      t.references :planet
      t.references :body_reaction

      t.timestamps
    end
  end
end
