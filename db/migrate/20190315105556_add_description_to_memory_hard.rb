class AddDescriptionToMemoryHard < ActiveRecord::Migration[5.2]
  def change
    add_column :memory_hard_choices, :emotion_name, :string
    add_column :memory_hard_choices, :emotion_description, :string
  end
end
