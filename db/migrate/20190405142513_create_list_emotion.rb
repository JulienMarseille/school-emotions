class CreateListEmotion < ActiveRecord::Migration[5.2]
  def change
    create_table :list_emotions do |t|
      t.string :name
    end
  end
end
