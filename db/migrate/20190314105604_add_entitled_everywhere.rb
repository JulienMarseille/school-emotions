class AddEntitledEverywhere < ActiveRecord::Migration[5.2]
  def change
    remove_column :links, :situation
    remove_column :connects, :question

    add_column :links, :entitled, :string
    add_column :body_reactions, :entitled, :string
    add_column :connects, :entitled, :string
    add_column :memory_beginners, :entitled, :string
    add_column :memory_hards, :entitled, :string
  end
end
