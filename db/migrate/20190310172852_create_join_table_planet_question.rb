class CreateJoinTablePlanetQuestion < ActiveRecord::Migration[5.2]
  def change
    create_table :planet_questions do |t|
      t.references :planet
      t.references :question
    end
  end
end
