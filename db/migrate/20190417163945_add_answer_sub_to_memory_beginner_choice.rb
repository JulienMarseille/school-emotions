class AddAnswerSubToMemoryBeginnerChoice < ActiveRecord::Migration[5.2]
  def change
    add_column :memory_beginner_choices, :answer_sub, :string
  end
end
