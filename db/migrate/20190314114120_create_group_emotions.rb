class CreateGroupEmotions < ActiveRecord::Migration[5.2]
  def change
    create_table :group_emotions do |t|
      t.string :emotion

      t.references :scratch
      t.timestamps
    end
  end
end
