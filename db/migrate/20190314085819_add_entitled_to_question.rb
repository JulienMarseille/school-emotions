class AddEntitledToQuestion < ActiveRecord::Migration[5.2]
  def change
    add_column :questions, :entitled, :string
  end
end
