class CreateBodyReactions < ActiveRecord::Migration[5.2]
  def change
    create_table :body_reactions do |t|
      t.string :name

      t.timestamps
    end
  end
end
