class AddChoiceToQuestions < ActiveRecord::Migration[5.2]
  def change
    add_reference :choices, :question
  end
end
