class CreatePlanetFree < ActiveRecord::Migration[5.2]
  def change
    create_table :planet_frees do |t|
      t.references :planet
      t.references :free

      t.timestamps
    end
  end
end
