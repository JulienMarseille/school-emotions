class CreatePlanetMemoryBeginners < ActiveRecord::Migration[5.2]
  def change
    create_table :planet_memory_beginners do |t|
      t.references :planet
      t.references :memory_beginner

      t.timestamps
    end
  end
end
