class CreatePlanetConnects < ActiveRecord::Migration[5.2]
  def change
    create_table :planet_connects do |t|
      t.references :planet
      t.references :connect

      t.timestamps
    end
  end
end
