class CreateFree < ActiveRecord::Migration[5.2]
  def change
    create_table :frees do |t|
      t.string :name
      t.string :entitled
    end
  end
end
