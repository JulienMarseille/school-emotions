class AddAnswerSubToChoice < ActiveRecord::Migration[5.2]
  def change
    add_column :choices, :answer_sub, :string
  end
end
