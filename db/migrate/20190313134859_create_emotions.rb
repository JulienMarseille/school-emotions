class CreateEmotions < ActiveRecord::Migration[5.2]
  def change
    create_table :emotions do |t|
      t.string :name

      t.references :body_reaction
      t.timestamps
    end
  end
end
