class AddExplanationToQuestion < ActiveRecord::Migration[5.2]
  def change
    add_column :questions, :explanation_title, :string
  end
end
