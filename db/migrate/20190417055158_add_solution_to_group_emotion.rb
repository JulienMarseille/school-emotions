class AddSolutionToGroupEmotion < ActiveRecord::Migration[5.2]
  def change
    add_column :group_emotions, :solution_title, :string
    add_column :group_emotions, :solution_subtitle, :string
  end
end
