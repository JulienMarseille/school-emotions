class CreateMemoryHardChoices < ActiveRecord::Migration[5.2]
  def change
    create_table :memory_hard_choices do |t|
      t.references :memory_hard

      t.timestamps
    end
  end
end
