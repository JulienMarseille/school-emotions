class AddMemoryBeginnerChoiceToMemoryBeginner < ActiveRecord::Migration[5.2]
  def change
    add_reference :memory_beginner_choices, :memory_beginner
  end
end
