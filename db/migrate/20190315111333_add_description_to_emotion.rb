class AddDescriptionToEmotion < ActiveRecord::Migration[5.2]
  def change
    add_column :reactions, :description, :string
  end
end
