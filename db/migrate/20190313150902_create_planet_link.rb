class CreatePlanetLink < ActiveRecord::Migration[5.2]
  def change
    create_table :planet_links do |t|
      t.references :planet
      t.references :link
    end
  end
end
