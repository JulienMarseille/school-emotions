class CreateMemoryBeginnerChoices < ActiveRecord::Migration[5.2]
  def change
    create_table :memory_beginner_choices do |t|
      t.string :answer
      t.timestamps
    end
  end
end
