class CreatePlanetScratches < ActiveRecord::Migration[5.2]
  def change
    create_table :planet_scratches do |t|
      t.references :planet
      t.references :scratch

      t.timestamps
    end
  end
end
