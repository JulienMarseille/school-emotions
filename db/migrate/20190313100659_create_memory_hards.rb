class CreateMemoryHards < ActiveRecord::Migration[5.2]
  def change
    create_table :memory_hards do |t|
      t.string :name

      t.timestamps
    end
  end
end
