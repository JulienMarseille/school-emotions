class CreateScratches < ActiveRecord::Migration[5.2]
  def change
    create_table :scratches do |t|
      t.string :name
      t.string :entitled

      t.timestamps
    end
  end
end
