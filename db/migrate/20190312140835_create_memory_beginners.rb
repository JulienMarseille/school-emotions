class CreateMemoryBeginners < ActiveRecord::Migration[5.2]
  def change
    create_table :memory_beginners do |t|
      t.string :name

      t.timestamps
    end
  end
end
