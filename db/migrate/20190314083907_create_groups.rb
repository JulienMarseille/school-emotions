class CreateGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :groups do |t|
      t.string :solution
      t.string :error

      t.references :link
      t.timestamps
    end
  end
end
