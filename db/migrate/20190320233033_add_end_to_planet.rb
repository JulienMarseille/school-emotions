class AddEndToPlanet < ActiveRecord::Migration[5.2]
  def change
    add_column :planets, :end_text, :string
  end
end
