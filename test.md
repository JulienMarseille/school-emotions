# Download the twilio-ruby library from twilio.com/docs/libraries/ruby
require 'twilio-ruby'

account_sid = 'AC97ffe58d84c80d9933885dce967cb2c3'
auth_token = '2bfe9e71d74c0f4fa4b6e5168bda00d3'
client = Twilio::REST::Client.new(account_sid, auth_token)

from = '+13182528093' # Your Twilio number
to = '+33643957806' # Your mobile phone number

client.messages.create(
from: from,
to: to,
body: "Hey friend!"
)
