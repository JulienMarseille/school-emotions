# frozen_string_literal: true

namespace :list_membres do
  desc "TODO"
  task seed_list: :environment do
    MembreCorp.create!(name: "bouche")
    MembreCorp.create!(name: "bras")
    MembreCorp.create!(name: "coeur")
    MembreCorp.create!(name: "dents")
    MembreCorp.create!(name: "epaule")
    MembreCorp.create!(name: "mains")
    MembreCorp.create!(name: "tete")
    MembreCorp.create!(name: "ventre")
  end
end
