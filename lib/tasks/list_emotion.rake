# frozen_string_literal: true

namespace :list_emotion do
  desc "TODO"
  task seed_list: :environment do
    ListEmotion.create!(name: "degout")
    ListEmotion.create!(name: "colère")
    ListEmotion.create!(name: "tristesse")
    ListEmotion.create!(name: "surprise")
    ListEmotion.create!(name: "peur")
    ListEmotion.create!(name: "joie")
  end
end
